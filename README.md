
# mkdir project
## 1. 初始化lerna项目 lerna init --independent(简写-i)

```js
  lerna notice cli v4.0.0
  lerna info Initializing Git repository // 初始化git仓库
  lerna info Creating package.json // 创建
  lerna info Creating lerna.json // 创建
  lerna info Creating packages directory // 创建packages目录
  lerna success Initialized Lerna files
```

### 创建包
```js
  lerna create lerna2 --registry http://localhost:4873
  lerna create @lerna2/cli --registry http://localhost:4873
  lerna create @lerna2/init --registry http://localhost:4873
  lerna create @lerna2/create --registry http://localhost:4873
```

### 添加依赖
```js
  // 1. 指定在 @lerna/cli 文件下安装lodash
  lerna add lodash --scope @lerna/cli
  // 2. 所有的依赖都安装在根目录下
    package.json中配置
    "workspaces": [
      "packages/*"
    ],
    lerna.json中配置
    "useWorkspaces": true,
    "npmClient": "yarn"
  lerna add vue -W
```

- packages/project/package.json/files作用
publish的时候是把哪个包提到npm上

#### 发布到私服上方式
  <!-- 需要先git add .  git commit -m"" -->
  <!-- lerna 是把所有更改的文件一次都提交 -->
  - 1. lerna publish --registry=url
  <!-- npm publish只能推送单个修改的文件 -->
  - 2. npm publish --registry=url
备注: url 如果不写的话默认找到的地址是 packages/name/package.json/publishConfig

### 单元测试
```js
 1. 需要全局安装jest 并且创建jest.config.js
 2. 单元测试的指令(前提是需要在packages文件中配置好)
  1) 在package中配置script后使用  lerna run test(测试全部) lerna run test --scope lerna2(只测lerna2文件)
  2) 第二种测试方式 lerna exec -- jest(测试全部) lerna exec --scope lerna2 -- jest(只测lerna2文件)
```