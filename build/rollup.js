// rollup的babel 插件
const rollup = require('rollup') // 引入rollup
const resolve = require('rollup-plugin-node-resolve') // 引入rollup
const image = require('@rollup/plugin-image') // 引入rollup
const terser = require('rollup-plugin-terser').terser // 压缩代码的插件
const commonjs = require('rollup-plugin-commonjs') // rollup默认支持es6的模块系统，需要支持commonjs的话需要这个插件
const vue = require('rollup-plugin-vue') // rollup默认支持es6的模块系统，需要支持commonjs的话需要这个插件
const babel = require('rollup-plugin-babel')
const postcss = require('rollup-plugin-postcss')
const scss = require('rollup-plugin-scss')
const fs = require('fs')
const path = require('path')

const packName = process.argv[2] // 拿到 npm run build packName
const extensions = ['.js', '.jsx', '.ts', '.tsx', '.vue']
async function build (packName) {
  const projectPath = `./packages/${packName}` // 子包所在的路劲

  // 输入的文件配置
  const inputOptions = {
    input: `${projectPath}/lib/index.js`,
    plugins: [
      resolve({
        customResolveOptions: {
          moduleDirectory: 'src'
        },
        extensions,
        mainFields: ['module', 'main', 'jsnext:main', 'browser']
      }),
      postcss({
        minimize: true,
        extract: true
      }),
      scss(),
      vue({
        css: true,
        compileTemplate: true
      }),
      babel({ // babel文件的设置，会读取根目录的babel.config.js文件配置
        runtimeHelpers: true,
        extensions,
        exclude: 'node_modules/**',
        presets: [
          '@vue/babel-preset-jsx',
          ['@babel/preset-env', {
            modules: false,
            targets: {
              browsers: ['iOS >= 8', 'Android >= 4']
            }
          }]
        ],
        plugins: [
          ['@babel/plugin-transform-runtime', {
            regenerator: true
          }]
        ]
      }),
      terser(),
      commonjs(),
      image()
    ]
  }

  const bundle = await rollup.rollup(inputOptions) // inputOptions放在这里

  return Promise.all([
    bundle.write({
      file: `${projectPath}/esm/index.js`,
      format: 'esm',
      name: `${packName}`
    }),
    bundle.write({
      file: `${projectPath}/umd/index.js`,
      format: 'umd',
      name: `${packName}`
    })
  ])
}

// 不打包的名单
const noBuildList = [
  'FVuetifyI18n',
  'MoneyUtils',
  'vuetify-preset'
]

if (packName) {
  build(packName)
} else {
  const rootPath = path.resolve(__dirname, '../packages')
  fs.readdirSync(rootPath)
    // 过滤，只保留文件夹
    .filter(item => fs.statSync(path.resolve(rootPath, item)).isDirectory())
    // 过滤 需要要打包的文件
    .filter(item => !noBuildList.includes(item))
    // 为每一个文件夹创建对应的配置
    .map(item => {
      return build(item)
    })
}
