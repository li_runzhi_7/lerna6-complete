---
home: true
heroImage: /FBS-logo.svg
heroText: Flash
tagline: 公共组件文档（💩持续更新中...）
actionText: 组件文档 →
actionLink: /components/
features:
- title: 贴近业务
  details: 除了提供基础组件外,也更贴近公司内部业务场景的组件。

- title: 技术实现
  details: 基于Vuetify 提供的基础组件进行了扩展。

- title: 效率与质量
  details: 提供方便调用的前端组件与UI设计资源,提高产出质量,产出效率,保证UI还原度。

---

仓库地址：
```
git@codeup.aliyun.com:flashexpress/express/fe/template/flash-components.git
```

