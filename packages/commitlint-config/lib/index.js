'use strict';
// 222
module.exports = {
    extends: [
        "@commitlint/config-conventional"
    ],
    rules: {
        'body-leading-blank': [1, 'always'],
        'body-max-line-length': [2, 'always', 100],
        'footer-leading-blank': [1, 'always'],
        'footer-max-line-length': [2, 'always', 100],
        'header-max-length': [2, 'always', 100],
        'scope-case': [2, 'always', 'lower-case'],
        'subject-case': [
          2,
          'never',
          ['sentence-case', 'start-case', 'pascal-case', 'upper-case']
        ],
        'subject-empty': [2, 'never'],
        'subject-full-stop': [2, 'never', '.'],
        'type-case': [2, 'always', ['lower-case', 'upper-case']],
        'type-empty': [2, 'never'],
        'type-enum': [
          2,
          'always',
          [
            'refactor', // 重构（即不是新增功能，也不是修改bug的代码变动）
            'feat', // 新功能（feature）
            'fix', // 修补bug
            'chore', // 构建过程或辅助工具的变动
            'docs', // 文档（不影响代码功能）
            'style', // 代码格式（不影响代码功能）
            'test', // 增加测试 (不影响代码功能)
            'UI', // 样式修改（不影响代码功能）
            'i18n' // 编辑国际化相关内容 (不影响代码功能)
          ]
        ]
      }
}
