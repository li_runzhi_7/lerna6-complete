# `@orange/eslint-config-javascript`

> TODO: 使用说明

### 前置依赖

> 1.eslint(7.12.1)
> 2.eslint-plugin-import
> 3.eslint-plugin-node
> 4.seslint-plugin-promise

### 使用

```js
npm install @orange/eslint-config-javascript --save-dev --registry=http://localhost:4873/

// or

yarn add @orange/eslint-config-javascript --dev --registry=http://localhost:4873/
```

### 配置

```js

extends: [
  '@orange/eslint-config-javascript'
]
```

### 规范细则

规范 Fork 自 "eslint-config-standard": "^16.0.2"，进行了定制化修改 主要规范见: https://standardjs.com/rules-zhcn.html
