'use strict';

module.exports = {
    parser: 'vue-eslint-parser',
  
    env: {
      browser: true,
      es6: true
    },
  
    parserOptions: {
      // 设置 js 的解析器为 babel-eslint
      parser: 'babel-eslint',
      ecmaVersion: 2021,
      // ECMAScript modules 模式
      sourceType: 'module',
      ecmaFeatures: {
        // 不允许 return 语句出现在 global 环境下
        globalReturn: false,
        // 开启全局 script 模式
        impliedStrict: true,
        jsx: true
      },
      // 即使没有 babelrc 配置文件，也使用 babel-eslint 来解析
      requireConfigFile: false,
      // 仅允许 import export 语句出现在模块的顶层
      allowImportExportEverywhere: false
    },
  
    plugins: ['vue'],
  
    rules: {
      /* ----------- @eslint-plugin-vue/base ----------- */
      'vue/comment-directive': 'error',
      'vue/experimental-script-setup-vars': 'error',
      'vue/jsx-uses-vars': 'error',
  
      /* ----------- @eslint-plugin-vue/essential ----------- */
      'vue/no-arrow-functions-in-watch': 'error',
      'vue/no-async-in-computed-properties': 'error',
      'vue/no-custom-modifiers-on-v-model': 'error',
      'vue/no-dupe-keys': 'error',
      'vue/no-dupe-v-else-if': 'error',
      'vue/no-duplicate-attributes': 'error',
      'vue/no-multiple-template-root': 'error',
      'vue/no-mutating-props': 'error',
      'vue/no-parsing-error': 'error',
      'vue/no-reserved-keys': 'error',
      'vue/no-shared-component-data': 'error',
      'vue/no-side-effects-in-computed-properties': 'error',
      'vue/no-template-key': 'error',
      'vue/no-textarea-mustache': 'error',
      'vue/no-unused-components': 'error',
      'vue/no-unused-vars': 'error',
      /**
       * @description 禁止在同一个元素上使用 v-if 和 v-for 指令
       */
      'vue/no-use-v-if-with-v-for': 'error',
      'vue/no-v-for-template-key': 'error',
      'vue/no-v-model-argument': 'error',
      'vue/require-component-is': 'error',
      'vue/require-prop-type-constructor': 'error',
      'vue/require-render-return': 'error',
      'vue/require-v-for-key': 'error',
      'vue/require-valid-default-prop': 'error',
      'vue/return-in-computed-property': 'error',
      'vue/use-v-on-exact': 'error',
      'vue/valid-template-root': 'error',
      'vue/valid-v-bind-sync': 'error',
      'vue/valid-v-bind': 'error',
      'vue/valid-v-cloak': 'error',
      'vue/valid-v-else-if': 'error',
      'vue/valid-v-else': 'error',
      'vue/valid-v-for': 'error',
      'vue/valid-v-html': 'error',
      'vue/valid-v-if': 'error',
      'vue/valid-v-model': 'error',
      'vue/valid-v-on': 'error',
      'vue/valid-v-once': 'error',
      'vue/valid-v-pre': 'error',
      'vue/valid-v-show': 'error',
      'vue/valid-v-slot': ['error', {
        allowModifiers: true // Vuetify中有很多item.<name> header.<name>,以.分割的命名
      }],
      'vue/valid-v-text': 'error',
  
      /* ----------- @eslint-plugin-vue/strongly-recommended ----------- */
      /**
       * @description vue组件中标签的顺序<template> <script> <style>
       */
      'vue/component-tags-order': ['error', {
        order: [['script', 'template'], 'style']
      }],
      // 'vue/no-lone-template': 'error',
      /**
       * @description 禁止将多个参数传递给作用域插槽
       */
      'vue/no-multiple-slot-args': 'error',
  
      /* ----------- @flash ----------- */
      'vue/attribute-hyphenation': 'error',
      'vue/html-closing-bracket-newline': 'error',
      'vue/html-closing-bracket-spacing': 'error',
      'vue/html-end-tags': 'error',
      'vue/html-quotes': 'error',
      'vue/html-self-closing': 'error',
      /**
       * @description 模板标签中每个标签最大属性限制为3个，超出换行，单行单个
       */
      'vue/max-attributes-per-line': ['error', {
        singleline: 3,
        multiline: {
          max: 1,
          allowFirstLine: false
        }
      }],
      /**
       * @description 标签的缩进规则 2个空格
       */
      'vue/html-indent': ['error', 2, {
        attribute: 1,
        baseIndent: 1,
        closeBracket: 0,
        alignAttributesVertically: true,
        ignores: []
      }],
      /**
       * @description 在多行元素的内容之前和之后需要换行
       */
      'vue/multiline-html-element-content-newline': ['error', {
        ignoreWhenEmpty: true,
        ignores: ['pre', 'textarea'], // 忽略元素
        allowEmptyLines: false
      }],
      'vue/mustache-interpolation-spacing': 'error',
      'vue/name-property-casing': 'error',
      'vue/no-multi-spaces': 'error',
      'vue/no-spaces-around-equal-signs-in-attribute': 'error',
      'vue/no-template-shadow': 'error',
      'vue/prop-name-casing': 'error',
      'vue/require-default-prop': 'error',
      'vue/require-prop-types': 'error',
      'vue/v-bind-style': 'error',
      'vue/v-on-style': 'error',
      /**
       * @description <template>模板中attributes属性的顺序
       * @see https://v3.vuejs.org/style-guide/#element-attribute-order-recommended
       */
      'vue/attributes-order': 'error',
      'vue/no-confusing-v-for-v-if': 'error',
      /**
       * @description options组件属性定义顺序
       * @see https://v3.vuejs.org/style-guide/#component-instance-options-order-recommended
       */
      'vue/order-in-components': 'error',
      /**
       * @description 禁止再模板中使用`this`
       */
      'vue/this-in-template': 'error',
      'vue/array-bracket-spacing': 'error',
      'vue/arrow-spacing': 'error',
      'vue/space-infix-ops': 'error',
      'vue/space-unary-ops': 'error',
      'vue/require-name-property': 'error',
      'vue/padding-line-between-blocks': 'warn',
      /**
       * @description 禁止在组件中使用保留字符
       */
      // 'vue/no-reserved-component-names': 'error',
      'vue/eqeqeq': 'error',
      'vue/component-name-in-template-casing': 'error'
    }
  }
  